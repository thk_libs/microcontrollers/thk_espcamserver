# thk_EspCamServer
Zunächst muss die Bibliothek im Sketch inkludiert werden.

    #include "thk_EspCamServer.h" // WIFI und HTTP-Server

Anschließend wird das WiFI-Netzwerk konfiguriert und (optional) der Name des HTTP-Ressource festgelegt.

    const char *ssid = "FRITZBOX";
    const char *password = "qqqqqqqq";
    const char *uri = "/stream";

Nun kann das Objekt erzeugt werden.

    thk_EspCamServer server(ssid, password, uri);


Für die Verwendung der Klasse ist ein sogenannter Handler erforderlich. Dieser regelt das Verhalten beim Aufrufen der Internetressource (bspw. beim eintippen von http://172.02.11/server im Browser innerhalb des lokalen Netzwerks).\
Hier kann für den Anfang die folgende Funktion außerhalb der setup() und loop() Routine erstellt werden:

    static esp_err_t get_helloworld_handler(httpd_req_t *req)
    {
    /* Send a simple response */
    Serial.println("Ein HTTP Request hat uns erreicht!");
    return ESP_OK;
    }

Hier erfolgt beim Aufrufen der Internetressource eine Textausgabe durch den ESP32 im Seriellen Monitor.


In der Setup Routine wird die Verbindung zum WiFi-Netzwerk aufgebaut

    server.connect(); 

und der Webserver mit einer Referenz auf den benutzerdefinierten Handler (hier unser HelloWorld Beispiel) aufgerufen.

    server.start_webserver(&get_helloworld_handler);




### Erläuterung des Skripts
Zunächst wird die erforderliche Bibliothek zum Starten des Servers inkludiert

    #include "thk_EspCamServer.h" // WIFI und HTTP-Server

Im Anschluss wird das Netzwerk eingerichtet

    const char *ssid = "iPhone von Vladislav";
    const char *password = "qqqqqqqq";

Optional kann auch die Internetressource unbenannt werden. Diese wird beim Aufruf des Servers benötigt.

    const char *uri = "/stream";


Server-Objekt erzeugen

    thk_EspCamServer server(ssid, password, uri);


Und mit dem WiFi-Netzwerk verbinden 

    server.connect(); 

Abschließend wird der Webserver gestartet. Dafür ist ein sogenannter Handler erforderlich. Dieser bildet die Antwort auf den Zugriff über HTTP.
Hierzu die Referenz entsprechend anpassen.

    server.start_webserver(&get_helloworld_handler);

Wird nun die Internetressource im lokalen Netzwerk geöffnet wird, erfolgt eine Ausgabe im Seriellen Monitor.