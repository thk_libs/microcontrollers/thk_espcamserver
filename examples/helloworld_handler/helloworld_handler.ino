// Beschreibung:
//  Beispiel Sketch für lokales Streamen des Kamerabildes per HTTP
// Abhängigkeiten:
// thk_EspCamServer.h
// Autor:
//  Vladislav Vlasuk (TH Köln, Labor für Assistenzsysteme)
// Datum:
//  05.07.2022

// Bitte ESP32 Kameramodell auskommentieren
#include <thk_EspCamServer.h> // WIFI und HTTP-Server

// Wlan-Netzwerk und Internetressource (URI) festlegen
const char *ssid = "iPhone von Vladislav";
const char *password = "qqqqqqqq";
const char *uri = "/stream";

// Objekte erzeugen
thk_EspCamServer server(ssid, password, uri);

void setup()
{
  Serial.begin(115200);

  server.connect();                            // Wifi verbinden
  server.start_webserver(&get_helloworld_handler); // Server starten
};

void loop()
{
  // Do nothing. Everything is done in another task by the web server
  delay(1e4);
}

// Bare Minimum Handler
static esp_err_t get_helloworld_handler(httpd_req_t *req)

{
  /* Send a simple response */
  Serial.println("Ein HTTP Request hat uns erreicht!");
  return ESP_OK;
}
