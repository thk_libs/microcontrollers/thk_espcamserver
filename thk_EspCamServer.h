#include "esp_http_server.h"
#include <WiFi.h>

// Von Espresif Systems (Shanghei) PTE LTD für Stream vorgegeben
#define PART_BOUNDARY "123456789000000000000987654321"
static const char *_STREAM_CONTENT_TYPE = "multipart/x-mixed-replace;boundary=" PART_BOUNDARY;
static const char *_STREAM_BOUNDARY = "\r\n--" PART_BOUNDARY "\r\n";
static const char *_STREAM_PART = "Content-Type: image/jpeg\r\nContent-Length: %u\r\nX-Timestamp: %d.%06d\r\n\r\n";

// Ermöglicht die Datenübertragung per WIFI mittels HTTP
class thk_EspCamServer
{
private:
    const char *ssid = NULL;
    const char *password = NULL;
    const char *uri = NULL;
    IPAddress address;

public:
    // Ein Server benötigt immer eine ssid, pw, uri
    thk_EspCamServer(const char *a_ssid, const char *a_pw, const char *a_uri) : ssid(a_ssid), password(a_pw), uri(a_uri)
    {
    }

    // Startet die Verbindung zum Netzwerk.
    bool connect()
    {
        WiFi.mode(WIFI_STA);
        WiFi.begin(ssid, password);
        WiFi.setSleep(false);
        Serial.print("Verbinde mit Netzwerk >");
        Serial.print(ssid);
        Serial.print("< [");
        while (WiFi.status() != WL_CONNECTED)
        {
            delay(500);
            Serial.print(".");
        }
        Serial.println("]");
        Serial.print("[OK] WiFi verbunden: ");
        Serial.println(WiFi.localIP());
        address = WiFi.localIP();
        return true;
    }

    // Startet für den URI Handler einen Server, der über HTTP erreicht werden kann
    // @param func Handler, welcher ausgeführt werden soll
    //
    httpd_handle_t start_webserver(esp_err_t (*func)(httpd_req_t *req))
    {
        /* Generate default configuration */
        httpd_config_t config = HTTPD_DEFAULT_CONFIG();

        /* Empty handle to esp_http_server */
        httpd_handle_t server = NULL;

        /* URI handler structure for GET /uri */
        httpd_uri_t uri_get = {
            .uri = this->uri,
            .method = HTTP_GET,
            .handler = func,
            .user_ctx = NULL};

        /* Start the httpd server */
        if (httpd_start(&server, &config) == ESP_OK)
        {
            Serial.printf("[OK] HTTP_Get-Server bereit ( Port %d ) HTTP: ", config.server_port);
            Serial.print(address);
            Serial.println(uri);

            /* Register URI handlers */
            httpd_register_uri_handler(server, &uri_get);
        }
        /* If server failed to start, handle will be NULL */
        return server;
    }

    /* Function for stopping the webserver */
    void stop_webserver(httpd_handle_t server)
    {
            /* Stop the httpd server */
            Serial.println("Schließe Server");
            httpd_stop(server);
    }
};
